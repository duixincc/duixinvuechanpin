import Vue from 'vue';
import {mapState,mapGetters} from 'vuex';
import App from './App';
import uView from '@/uni_modules/uview-ui'
import GoEasy from '@/uni_modules/GOEASY-IM/js_sdk/goeasy-2.8.3.esm.min.js'
import config from "utils/config";
import store from './store';
// #ifdef H5
 const wx = require('jweixin-module');
// #endif
var wss = config.getWss;
Vue.use(uView);

Vue.config.productionTip = false;

Vue.mixin({
	data() {
	  return {
	  };
	},
	onLoad() {
	},
	computed: {
		/*...mapState({
		  themeName: 'themeName'
		}),*/
		...mapGetters({
		  theme: "theme"
		})
	},
	methods: {
		setData: function(obj, callback) {
			let that = this;
			const handleData = (tepData, tepKey, afterKey) => {
				tepKey = tepKey.split('.');
				tepKey.forEach(item => {
					if (tepData[item] === null || tepData[item] === undefined) {
						let reg = /^[0-9]+$/;
						tepData[item] = reg.test(afterKey) ? [] : {};
						tepData = tepData[item];
					} else {
						tepData = tepData[item];
					}
				});
				return tepData;
			};
			const isFn = function(value) {
				return typeof value == 'function' || false;
			};
			Object.keys(obj).forEach(function(key) {
				let val = obj[key];
				key = key.replace(/\]/g, '').replace(/\[/g, '.');
				let front, after;
				let index_after = key.lastIndexOf('.');
				if (index_after != -1) {
					after = key.slice(index_after + 1);
					front = handleData(that, key.slice(0, index_after), after);
				} else {
					after = key;
					front = that;
				}
				if (front.$data && front.$data[after] === undefined) {
					Object.defineProperty(front, after, {
						get() {
							return front.$data[after];
						},
						set(newValue) {
							front.$data[after] = newValue;
							that.$forceUpdate();
						},
						enumerable: true,
						configurable: true
					});
					front[after] = val;
				} else {
					that.$set(front, after, val);
				}
			});
			// this.$forceUpdate();
			isFn(callback) && this.$nextTick(callback);
		}
	}
});

App.mpType = 'app';

const app = new Vue({
	store,
    ...App
});
app.$mount();
const goEasy = GoEasy.getInstance({
    host:"hangzhou.goeasy.io",  //若是新加坡区域：singapore.goeasy.io
    appkey:"BC-f535642e1beb474b9d6102ca009c8e9a", //申请地址：goeasy.io  BC-f535642e1beb474b9d6102ca009c8e9a
    modules:['im']//根据需要，传入'im’或'pubusub'，或数组方式同时传入
});
// 创建websocket
var ws,
// #ifdef H5
  ws = new WebSocket(wss);
// #endif

// #ifdef APP-PLUS || MP-WEIXIN

	ws = uni.connectSocket({
	  url: wss,
	  complete: ()=> {},
	  success(data) {
	     console.log("websocket连接成功",data);
	  },
	  fail(err) {
		console.error('WebSocket连接失败', err);
	  }
	});
// #endif
Vue.prototype.goEasy = goEasy;
Vue.prototype.GoEasy = GoEasy;
Vue.prototype.duixinim = ws;
// #ifdef H5
 Vue.prototype.$wx = wx;
// #endif
//Vue.prototype.$zhuti = zhuti;