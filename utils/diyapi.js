
import config from "./config";
var domain = config.getDomain; //域名
var DUIXIN_CATS_ID = config.getDUIXIN_CATS_ID; //接口id
var pageCount = config.getPageCount;//接口列表数量，
var apileixing = config.getApileixing; //接口类型httpapi
//接口密钥
var DUIXIN_API =  domain + '/index.php?v=1&appid='+config.getAPPID+'&appsecret='+config.getAPPSECRET+'&';

module.exports = {
	// DiyApi 接口方法 下面的 getDiyApi 为例子。自己命名接口的名称即可
	// 例子：https://duixin.cc/quan/536.html
	getDiyApi: function() {
		return DUIXIN_API+'&s='+apileixing+'&m=site';
	},
	
};