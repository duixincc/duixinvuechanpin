
import config from "./config";
import configfabu from "./configfabu";

var domain = config.getDomain;
var h5 = config.getH5;
var DUIXIN_BANNER_ID = config.getDUIXIN_BANNER_ID;
var DUIXIN_CATS_ID = config.getDUIXIN_CATS_ID;
var pageCount = config.getPageCount;
var categoriesID = config.getCategoriesID;
var apileixing = config.getApileixing;

var jiugetu = configfabu.postDUIXIN_JIUGETU_ID;
var DUIXIN_API =  domain + '/index.php?v=1&appid='+config.getAPPID+'&appsecret='+config.getAPPSECRET+'&';

module.exports = {
	// 站点
	getWwwUrl: function(obj,uid,code) {
		//console.log('obj',obj); 
		var url = DUIXIN_API;
		if (obj) {
		  url += '&api_auth_uid='+uid+'&api_auth_code='+code;
		}
		return url;
	},
	getWww: function() {
		return DUIXIN_API+'&s='+apileixing+'&m=site';
	},
	// banner轮播图 
	getSwiperUrl: function() {
		return DUIXIN_API+'&s='+apileixing+'&id='+DUIXIN_BANNER_ID;
	},
	// 首页图标
	getTubiaoUrl: function () {
	  var url = DUIXIN_API + '&s=tubiao&c=search&api_call_function=module_list&pagesize';
	  return url;
	},
	// 圈子首页列表
	getQuanindexUrl: function (obj,uid) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=3&uid='+uid+'&page=' + obj.page;
	  //console.log('obj',obj.guanzhuuid); 
	  if (obj.guanzhuuid) {
	    url += '&guanzhuuid='+obj.guanzhuuid;
	  }
	  if (obj.unuid) {
	    url += '&unuid='+obj.unuid;
	  }
	  return url;
	},
	// 圈子列表
	getQuanlistUrl: function () {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=4';
	  return url;
	},
	// 热门列表
	getRemenlistUrl: function (obj) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=5'+ '&page=' + obj.page;
	  if (obj.uid > 0) {
	    url += '&uid=' + obj.uid;
	  }
	  return url;
	},
	// 文章列表
	getListUrl: function (obj) {
	  var url = DUIXIN_API + '&s=news&c=search&api_call_function=module_list&pagesize=' + pageCount + '&page=' + obj.page;
	  if (obj.categories != 0) {
	    url += '&catid=' + obj.categories;
	  } else if (obj.search != '') {
	    url += '&keyword=' + encodeURIComponent(obj.search);
	  }
	  return url;
	},
	getTopUrl: function (obj) {
	  var url = DUIXIN_API + '&order=hits&s=news&c=search&api_call_function=module_list&&pagesize=' + pageCount + '&page=' + obj.page;
	
	return url;
	},
	// 文章内容页
	getShowUrl: function (id) {
		return DUIXIN_API + '&s=news&c=show&api_call_function=module_show&id=' + id;
	},  
	//获取分类列表
	getCategoryUrl: function (ids) {
		return DUIXIN_API+'&s='+apileixing+'&id='+DUIXIN_CATS_ID;
	},
	//用户登录
	postYonghuLogin: function () {
		return DUIXIN_API+'&s=member&c=login';
	},
	// 微信H5登录
	postWeixinlogin: function (code,state) {
		return DUIXIN_API+'&s=weixin&c=member&m=weixingongzhonghaologin&code='+code+'&state='+state;
	},
	// 微信公众号H5登录
	postWeixingongzhonghaologin: function (url) {
		return DUIXIN_API+'&s=weixin&c=go&m=weixin&url='+url;
	},
	//用户注册
	postYonghuRegiste: function () {
		return DUIXIN_API+'&s=member&c=register&m=index';
	},
	//用户信息
	getYonghuXinxi: function (id) {
		return DUIXIN_API+'&s='+apileixing+'&m=member&uid='+id;
	},
	//用户头像
	getTouxiang(uid,ip) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=32&uid='+uid+'&ip='+ip;
	  return url;
	},
	//获取小微信程序登录
	postWeixinLogin: function (ids) {
		return DUIXIN_API+'&s=weixin&c=member&m=xcx';
	},
	//账号绑定小程序
	postWeixinBangdingYicun: function (ids) {
		return DUIXIN_API+'&s=weixin&c=member&m=xcx_bangyicun';
	},
	//小微信程序绑定账号
	postWeixinBangding: function () {
		return DUIXIN_API+'&s=weixin&c=member&m=xcx_bang';
	},
	//上传头像
	postTouxiang: function (uid,code) {
		return DUIXIN_API+'&s=member&c=account&m=avatar&api_auth_uid='+uid+'&api_auth_code='+code;
	},
	//上传图片多张
	postTupians: function (uid,code) {
		return DUIXIN_API+'s=api&c=file&m=upload&api_auth_uid='+uid+'&api_auth_code='+code+'&siteid=1';
	},
	//普通帖子id
	getTieidPt: function (tieid,uid,code) {
		return DUIXIN_API+'s=httpapi&id=6&api_auth_code='+code+'&api_auth_uid='+uid+'&tieid='+tieid;
	},
	//带会员参数帖子id
	getTieid: function (quanid,tieid,uid,code) {
		return DUIXIN_API+'s=quan&c=show&id='+quanid+'&api_auth_code='+code+'&api_auth_uid='+uid+'&tieid='+tieid;
	},
	//评论内容展示+帖子内容展示
	getPinglunNeirong: function (tieid,uid,code) {
		return DUIXIN_API+'s=quan&c=son_comment&id='+tieid+'&api_auth_code='+code+'&api_auth_uid='+uid;
	},
	// 提交评论
	postPinglun: function (tieid,uid,code,rid) {
		return DUIXIN_API+'s=quan&c=son_comment&m=post&id='+tieid+'&api_auth_code='+code+'&api_auth_uid='+uid+'&rid='+rid;
	},
	//点赞
	getZan: function (tieid,uid,code) {
		return DUIXIN_API+'s=quan&c=son&m=zan&id='+tieid+'&api_auth_code='+code+'&api_auth_uid='+uid;
	},
	//点赞值
	getZanzhi: function (tieid,uid,code) {
		return DUIXIN_API+'s=quan&c=son&m=zan&id='+tieid+'&api_auth_code='+code+'&api_call_function=zan&uid='+uid;
	},
	//收藏
	getShoucang: function (tieid,uid,code) {
		return DUIXIN_API+'s=quan&c=son&m=quan&id='+tieid+'&api_auth_code='+code+'&api_auth_uid='+uid;
	},
	//首页置顶
	getZhiding: function () {
		var url = DUIXIN_API + '&s='+apileixing+'&id=6';
		return url;
	},
	//圈子
	getQuan: function (quanid,qid,erji) {
		var url = DUIXIN_API + '&s=quan&c=show&id='+ quanid +'&qid='+qid+'&erji='+erji+'&api_call_function=module_show';
		
		return url;
	},
	//独立圈子列表
	getQuanlist: function (obj,uid,quanid,qid,erji) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=7&uid='+uid+'&quanid='+quanid+'&qid='+qid+'&erji='+erji+'&page=' + obj.page;
	  if (obj.unuid) {
	    url += '&unuid='+obj.unuid;
	  }
	  return url;
	},
	// 圈子搜索列表
	getQuansousuo: function (obj,k) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=8&k='+k+'&page=' + obj.page;
	  return url;
	},
	// 圈子新评列表
	getQuanxinping: function (obj,k) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=9&k='+k+'&page=' + obj.page;
	  return url;
	},
	// 新帖列表
	getXintielistUrl: function (obj) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=10'+ '&page=' + obj.page;
	  if (obj.uid > 0) {
	    url += '&uid=' + obj.uid;
	  }
	  return url;
	},
	// 首页签到
	getQiandao: function (uid,code) {
	  var url = DUIXIN_API + 'is_ajax=1&s=member&app=qiandao&c=home&m=dosign&quan=0&api_auth_uid='+uid+'&api_auth_code='+code;
	  /*if (obj.uid > 0) {
	    url += '&uid=' + obj.uid;
	  }*/
	  return url;
	},
	// 首页签到会员
	getQiandaohuiyuan: function () {
	  var url = DUIXIN_API + 's='+apileixing+'&id=11';
	  /*if (obj.uid > 0) {
	    url += '&uid=' + obj.uid;
	  }*/
	  return url;
	},
	//圈子首页置顶
	getQuanZhiding: function (cid) {
		var url = DUIXIN_API + '&s='+apileixing+'&id=12&quanid='+cid;
		return url;
	},
	//推荐圈子
	getTuijianQuanzi: function () {
		var url = DUIXIN_API + '&s='+apileixing+'&id=15';
		return url;
	},
	//统计
	getTongji: function () {
		var url = DUIXIN_API + '&s='+apileixing+'&id=16';
		return url;
	},
	//发布帖子
	postTiezi: function (uid,code,cid) {
		return DUIXIN_API + '&s=member&app=quan&c=son&m=add&api_auth_uid='+uid+'&api_auth_code='+code+'&cid='+cid;
	},
	//全文搜索接口
	getJiansuoTiezi: function () {
		return DUIXIN_API + 's=jiansuo&c=home&m=index&lei=1';
	},
	//个人页面
	getGerenyemian: function (obj,uid,code,guid) {
		var url = DUIXIN_API  + '&s='+apileixing+'&id=17&uid='+uid+'&guid='+guid+'&page=' + obj.page;
		if (obj) {
		  url += '&api_auth_uid='+uid+'&api_auth_code='+code;
		}
		return url;
	},
	// 用户认证
	getRenzheng: function (uid,code) {
	  var url = DUIXIN_API + 's=member&app=certify&c=home&m=index&api_auth_uid='+uid+'&api_auth_code='+code;
	  /*if (obj.uid > 0) {
	    url += '&uid=' + obj.uid;
	  }*/
	  return url;
	},
	// 修改资料
	postZiliao: function (uid,code) {
	  var url = DUIXIN_API + 's=member&c=account&m=index&api_auth_uid='+uid+'&api_auth_code='+code;
	  /*if (obj.uid > 0) {
	    url += '&uid=' + obj.uid;
	  }*/
	  return url;
	},
	//信息推送
	getTuisongxinxi: function () {
		var url = DUIXIN_API + '&s='+apileixing+'&id=18';
		return url;
	},
	// tag列表
	getTagList: function (obj) {
	  var url = DUIXIN_API + '&s=tags&c=search&api_call_function=module_list&pagesize=' + pageCount + '&page=' + obj.page;s
	  if (obj.categories != 0) {
	    url += '&catid=' + obj.categories;
	  } else if (obj.search != '') {
	    url += '&keyword=' + encodeURIComponent(obj.search);
	  }
	  return url;
	},
	//热门Tags推送
	getTags: function () {
		var url = DUIXIN_API + '&s='+apileixing+'&id=19';
		return url;
	},
	// Tags列表
	getTagslist: function (obj,k) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=20&tag='+k+'&page=' + obj.page;
	  return url;
	},
	// 用户认证
	getRenzhengs: function (uid) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=21&uid='+uid;
	  return url;
	},
	// 关注
	getGuanzhu(uid,code) {
		var url = DUIXIN_API + '&s=huiyuan&c=show&m=friend&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 内容页面的个人关注
	getGerenGuanzhu(uid,code,touid) {
		var url = DUIXIN_API + '&s='+apileixing+'&id=22&uid='+uid+'&touid='+touid+'&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 进度条
	getJindu: function (id) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=23&tid='+id;
	  return url;
	},
	// 进度条设置
	getJindudata(uid,code,id,quanid,leixing){
		var url = DUIXIN_API + '&s=quan&c=son&m=jindudata&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid+'&leixing='+leixing;
		return url;
	},
	//通知
	getTongzhi(uid,catid,obj){
		var url = DUIXIN_API + '&s='+apileixing+'&id=25&uid='+uid+'&catid='+catid+'&page='+obj.page;
		return url;
	},
	//已读
	getYidus: function (tieid,uid,code) {
		return DUIXIN_API+'s=quan&c=son&m=yidu&id='+tieid+'&api_auth_code='+code+'&api_auth_uid='+uid;
	},
	// 消息统计
	getXiaoxitongji(uid) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=26&uid='+uid;
	  return url;
	},
	//举报
	postJubao: function (uid,code,id) {
		return DUIXIN_API + '&s=jubao&c=post&m=index&moxing=quan&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id;
	},
	//加入圈子
	postJiaruquanzi(uid,code,id) {
		return DUIXIN_API + '&s=quan&c=show&m=cart&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id;
	},
	//判断加入圈子
	//加入圈子
	getInquanzi(uid,code,id) {
		var url = DUIXIN_API + '&s='+apileixing+'&id=27&uid='+uid+'&cid='+id+'&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	//(新接口)加入圈子
	getJiaruQuanzi(uid,code,id) {
		var url = DUIXIN_API + '&s=quan&c=show&m=jiaruquanzi&uid='+uid+'&cid='+id+'&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	//添加私信
	postAddtext(uid,code) {
		return DUIXIN_API + '&s=duixin&c=duixin&m=addtext&api_auth_uid='+uid+'&api_auth_code='+code;
	},
	//已读
	getYidu(uid,code,touid) {
		return DUIXIN_API + '&s=duixin&c=duixin&m=yidu&api_auth_uid='+uid+'&api_auth_code='+code+'&touid='+touid;
	},
	//列表
	getDuixinlist(uid,code) {
		return DUIXIN_API + '&s=duixin&c=duixin&m=list&api_auth_uid='+uid+'&api_auth_code='+code;
	},
	//判断好友
	getIshaoyou(uid,code,touid){
		return DUIXIN_API + '&s=duixin&c=duixin&m=ishaoyou&api_auth_uid='+uid+'&api_auth_code='+code+'&touid='+touid;
	},
	//判断好友
	postHujia(uid,code,touid){
		return DUIXIN_API + '&s=duixin&c=duixin&m=hujia&api_auth_uid='+uid+'&api_auth_code='+code+'&touid='+touid;
	},
	//认证添加好友
	postRenzhenghaoyou(uid,code,touid){
		return DUIXIN_API + '&s=duixin&c=duixin&m=renzhenghujia&api_auth_uid='+uid+'&api_auth_code='+code+'&touid='+touid;
	},
	// 帖子点赞统计
	getDianzantongji(id) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=28&tieid='+id;
	  return url;
	},
	//审核帖子
	getShenhetiezi(uid,code,id){
		return DUIXIN_API + '&s=quan&c=son&m=shenhe&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id;
	},
	//帮助导航
	getBangzhudaohang(){
		var url = DUIXIN_API + '&s=httpapi&m=category&mid=bang&pid=0';
		return url;
	},
	//帮助列表
	getBangzhulist(obj){
		var url = DUIXIN_API + '&s=bang&c=search&api_call_function=module_list&pagesize=' + pageCount + '&page=' + obj.page;
		if (obj.categories != 0) {
		url += '&catid=' + obj.categories;
		} else if (obj.search != '') {
		url += '&keyword=' + encodeURIComponent(obj.search);
		}
		return url;
	},
	//帮助内容
	getBangzhushow(id){
		return DUIXIN_API + '&s=bang&c=show&api_call_function=module_show&id='+id;
	},
	//反馈导航
	getFankuidaohang(){
		var url = DUIXIN_API + '&s=httpapi&m=category&mid=fankui&pid=0';
		return url;
	},
	//反馈列表
	getFankuilist(obj){
		var url = DUIXIN_API + '&s=fankui&c=search&api_call_function=module_list&pagesize=' + pageCount + '&page=' + obj.page;
		if (obj.categories != 0) {
		url += '&catid=' + obj.categories;
		} else if (obj.search != '') {
		url += '&keyword=' + encodeURIComponent(obj.search);
		}
		return url;
	},
	//反馈内容
	getFankuishow(id){
		return DUIXIN_API + '&s=fankui&c=show&api_call_function=module_show&id='+id;
	},
	//提交反馈
	postFankui(uid,keycode){
		return DUIXIN_API + '&s=member&app=fankui&c=home&m=add&api_auth_uid='+uid+'&api_auth_code='+keycode;
	},
	//建议导航
	getJianyidaohang(){
		var url = DUIXIN_API + '&s=httpapi&m=category&mid=jianyi&pid=0';
		return url;
	},
	//建议列表
	getJianyilist(obj){
		var url = DUIXIN_API + '&s=jianyi&c=search&api_call_function=module_list&pagesize=' + pageCount + '&page=' + obj.page;
		if (obj.categories != 0) {
		url += '&catid=' + obj.categories;
		} else if (obj.search != '') {
		url += '&keyword=' + encodeURIComponent(obj.search);
		}
		return url;
	},
	//建议内容
	getJianyishow(id){
		return DUIXIN_API + '&s=jianyi&c=show&api_call_function=module_show&id='+id;
	},
	//建议反馈
	postJianyi(uid,keycode){
		return DUIXIN_API + '&s=member&app=jianyi&c=home&m=add&api_auth_uid='+uid+'&api_auth_code='+keycode;
	},
	//举报导航
	getJubaodaohang(){
		var url = DUIXIN_API + '&s=httpapi&m=category&mid=jubao&pid=0';
		return url;
	},
	//举报列表
	getJubaolist(obj){
		var url = DUIXIN_API + '&s=jubao&c=search&api_call_function=module_list&pagesize=' + pageCount + '&page=' + obj.page;
		if (obj.categories != 0) {
		url += '&catid=' + obj.categories;
		} else if (obj.search != '') {
		url += '&keyword=' + encodeURIComponent(obj.search);
		}
		return url;
	},
	// 问答进度条
	getWenda: function (id) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=29&tid='+id;
	  return url;
	},
	// 问答答案列表
	getWendalist: function (id,data) {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=30&tid='+id;
	  return url;
	},
	// 问答答案单独展示内容
	getWendadaan(id,uid,keycode) {
	  var url = DUIXIN_API + '&s=quan&c=wenda&m=index&id='+id+'&api_auth_uid='+uid+'&api_auth_code='+keycode;
	  return url;
	},
	// 问答奖励 和 中标
	getWendajiangli: function (uid,code,id,leixing,tieuid) {
	  var url = DUIXIN_API + '&s=quan&c=wenda&m=jianglijifen&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&leixing='+leixing+'&uid='+tieuid;
	  return url;
	},
	//问答答案提交
	postWendadaan(uid,code,cid,tid){
		var url = DUIXIN_API + '&s=member&app=quan&c=wenda&m=adddaan&api_auth_uid='+uid+'&api_auth_code='+code+'&cid='+cid+'&tid='+tid;
		return url;
	},
	//审核提交的答案
	postDananshenhe(uid,code,id,tid,touid){
		var url = DUIXIN_API + '&s=quan&c=wenda&m=daanshenhe&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&tid='+tid+'&uid='+touid;
		return url;
	},
	//审核提交的答案
	getDananzhuangtai(id){
		var url = DUIXIN_API + '&s='+apileixing+'&id=33&tid='+id;
		return url;
	},
	//招聘
	getZhaopin(id){
		var url = DUIXIN_API + '&s='+apileixing+'&id=31&tid='+id;
		return url;
	},
	//招聘确认
	getZhaopinqueren(uid,code,id,quanid){
		var url = DUIXIN_API + '&s=quan&c=son&m=zhaopin&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid;
		return url;
	},
	//bug进度
	getBug(uid,code,id,quanid){
		var url = DUIXIN_API + '&s=quan&c=son&m=bug&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid;
		return url;
	},
	// bug进度条设置
	getBugdata(uid,code,id,quanid,leixing){
		var url = DUIXIN_API + '&s=quan&c=son&m=bugdata&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid+'&leixing='+leixing;
		return url;
	},
	//需求进行
	getXuqiu(uid,code,id,quanid){
		var url = DUIXIN_API + '&s=quan&c=son&m=jindu&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid;
		return url;
	},
	//圈子置顶
	getQuanzhiding(uid,code,id,quanid){
		var url = DUIXIN_API + '&s=quan&c=son&m=zhiding&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid;
		return url;
	},
	//首页置顶
	getShouyezhiding(uid,code,id,quanid){
		var url = DUIXIN_API + '&s=quan&c=son&m=shouyezhiding&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid;
		return url;
	},
	//帖子精华
	getTiejinghua(uid,code,id,quanid){
		var url = DUIXIN_API + '&s=quan&c=son&m=jinghua&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid;
		return url;
	},
	//全站置顶
	getQuanzhanzhiding(uid,code,id,quanid){
		var url = DUIXIN_API + '&s=quan&c=son&m=qzzd&api_auth_uid='+uid+'&api_auth_code='+code+'&id='+id+'&quanid='+quanid;
		return url;
	},
	// 一键读取系统消息
	getYijiantongzhi(uid,code){
		var url = DUIXIN_API + '&is_ajax=1&s=member&app=duixin&c=home&m=yijianduquxingtong&api_auth_uid='+uid+'&api_auth_code='+code+'&uid='+uid;
		return url; 
	},
	// 一键读取点赞消息
	getYijiangdianzan(uid,code){
		var url = DUIXIN_API + '&is_ajax=1&s=member&app=duixin&c=home&m=yijiandianzan&api_auth_uid='+uid+'&api_auth_code='+code+'&uid='+uid;
		return url; 
	},
	// 一键读取评论消息
	getYijianpinglun(uid,code){
		var url = DUIXIN_API + '&is_ajax=1&s=member&app=duixin&c=home&m=yijianpinglun&api_auth_uid='+uid+'&api_auth_code='+code+'&uid='+uid;
		return url; 
	},
	//加入圈子
	getJiaruQuanlistUrl: function () {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=35';
	  return url;
	},
	// 我的圈子列表
	getWoQuanlistUrl: function () {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=34';
	  return url;
	},
	//实名发布
	getShimingfabu(uid,code){
		var url = DUIXIN_API + '&is_ajax=1&s=member&app=quan&c=son&m=shiming&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	//实名回复
	getShiminghuifu(uid,code){
		var url = DUIXIN_API + '&is_ajax=1&s=member&app=quan&c=son&m=huifu&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 站点信息
	getWwwXinxi() {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=36';
	  return url;
	},
	//微信客服
	getWeixinkefu() {
		var url = DUIXIN_API + '&s='+apileixing+'&id=37';
		return url;
	},
	//帖子视频内容
	getShipin(id){
		var url = DUIXIN_API + '&s='+apileixing+'&id=38&tid='+id;
		return url;
	},
	// 会员圈贴图标
	getQuantietuApi: function () {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=39';
	  return url;
	},
	// 活动二维码认证
	getHuodongrenzheng(uid,code){
		var url = DUIXIN_API + '&is_ajax=1&s=member&app=quan&c=hdbm&m=renzheng&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 圈子属性设置
	getQuanzishuxing() {
	  var url = DUIXIN_API + '&s='+apileixing+'&id=40';
	  return url;
	},
	//mp3
	getMpsan(id){
		var url = DUIXIN_API + '&s='+apileixing+'&id=42&tid='+id;
		return url;
	},
	// 微信分享密钥
	getWeixinfengxiangmiyue(urljia) {
		var url = DUIXIN_API + '&s=weixin&c=miyue&m=index&urljia='+urljia;
		return url;
	},
	// 微信分享图
	getWeixinfengxiangtu(tid) {
		var url = DUIXIN_API + '&s=weixin&c=miyue&m=tuwen&id='+tid;
		return url;
	},
	// 充值说明
	getPayshuoming(){
		var url = DUIXIN_API + '&s=pay&c=home&m=shuoming';
		return url;
	},
	// 充值
	postPay(uid,code){
		var url = DUIXIN_API + 's=member&c=pay&m=index&api_auth_uid='+uid+'&api_auth_code='+code+'&';
		return url;
	},
	// 充值订单
	getPayid(payid,urldata){
		var url = DUIXIN_API + 's=api&c=pay&id='+payid+'&'+urldata;
		return url;
	},
	// h5充值订单
	getH5Payid(payid,urldata){
		var url = domain + '/index.php?s=api&c=pay&id='+payid+'&'+urldata;
		return url;
	},
	// pc充值订单
	getPcPayid(payid,urldata){
		var url = domain + '/index.php?s=api&c=pay&id='+payid+'&'+urldata;
		return url;
	},
	// 会员
	getMemberid(uid,code){
		var url = DUIXIN_API + 's=member&c=home&m=index&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 用户组
	getYonghuzu(uid,code,hid){
		var url = DUIXIN_API + 's=member&app=quan&c=yonghuzu&m=index&api_auth_uid='+uid+'&api_auth_code='+code+'&hid='+hid;
		return url;
	},
	// 用户组
	getFufeijiaquan(uid,code,quanid){
		var url = DUIXIN_API + 's=member&app=quan&c=jiaru&m=fufeijiaquan&api_auth_uid='+uid+'&api_auth_code='+code+'&quanid='+quanid;
		return url;
	},
	// 拉黑用户
	getLaheiyonghu(uid,code){
		var url = DUIXIN_API + 's=member&app=quan&c=lahei&m=laheiuid&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 屏蔽帖子
	getLaheitiezi(uid,code){
		var url = DUIXIN_API + 's=member&app=quan&c=lahei&m=laheitiezi&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 屏蔽评论
	getLaheipinglun(uid,code){
		var url = DUIXIN_API + 's=member&app=quan&c=lahei&m=laheipinglun&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 解除用户
	postJiechuyonghu(uid,code){
		var url = DUIXIN_API + '&s=member&app=quan&c=lahei&m=jiefenguid&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 解除帖子
	postJiechutiezi(uid,code){
		var url = DUIXIN_API + '&s=member&app=quan&c=lahei&m=jiefengtiezi&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 解除评论
	postJiechupinglun(uid,code){
		var url = DUIXIN_API + '&s=member&app=quan&c=lahei&m=jiefengpinglun&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
	// 拉黑用户列表
	getJiechuyonghulist(uid){
		var url = DUIXIN_API + '&s='+apileixing+'&id=44&uid='+uid;
		return url;
	},
	// 屏蔽帖子列表
	getJiechutiezilist(uid){
		var url = DUIXIN_API + '&s='+apileixing+'&id=45&uid='+uid;
		return url;
	},
	// 屏蔽评论列表
	getJiechupinglunlist(uid){
		var url = DUIXIN_API + '&s='+apileixing+'&id=46&uid='+uid;
		return url;
	},
	//发布九格图id
	getFabuid(){
		var url = DUIXIN_API + '&s=quan&c=fabu&m=fabuid';
		return url;
	},
	// 用户背景图
	postUidbeijingtu(uids,uid,code){
		var url = DUIXIN_API + '&s=huiyuan&c=home&m=beijingtu&uid='+uids+'&api_auth_uid='+uid+'&api_auth_code='+code;
		return url;
	},
};