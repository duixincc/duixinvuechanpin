// 配置域名,域名只修改此处。duixin.cc // 小程序wx6f6dd9ea10ccc64d
var YUMING = "duixin.cc"

// 网站名称：对信开源社区
var WEBSITENAME = "对信开源社区"; 

// 接口appid
var DUIXIN_APPID = "1"; 

// 接口密钥 appsecret//	DUIXIN2FBBBAAB652DC 
var DUIXIN_APPSECRET = "DUIXIN2FBBBAAB652DC";
// 密钥需要独立购买。费用为50元。购买方法：登录网站后台，首页》应用商城 》 api接口 

//签到开启
var QIANDAOKAI = '1'; //说明：签到开关，1为打开。0为关闭
//签到是独立插件，费用50元。在网站后台的（首页》应用商城》签到）购买。购买后才能开启。

//全文检索开启
var JIANSUOKAI = 0; //说明：全文开关，1为打开。0为关闭
//全文检索是独立插件，费用200元。在网站后台的（首页》应用商城》全文检索）购买。购买后才能开启。

// 版本号 

var DUIXINBANBEN = 'v 2.5.0 - 20240503';

/*
**api类型：目前api类型：httpapi 
*/
var APILEIXING = 'httpapi'; //api类型
//设置downloadFile合法域名,不带https ,在中括号([])里增加域名，格式：{id=**,domain:'www.**.com'}，用英文逗号分隔。
//此处设置的域名和小程序与小程序后台设置的downloadFile合法域名要一致。

// 默认https
var DOMAIN = "https://"+YUMING;
//h5栏目名称
var H5 = "/m";
//wss服务端
var WSS = "wss://"+YUMING+"/wss";

// 调用栏目的接口的id
var DUIXIN_CATS_ID = "1"; 

// 幻灯接口的id号
var DUIXIN_BANNER_ID = "1"; 

//每页文章数目
var PAGECOUNT = '10'; 

//微信赞赏
var ZANIMAGEURL = ''; 

//开发者
var KAIFAZHE = '0';  // 1为开启 / 0为关闭

var DOWNLOADFILEDOMAIN = [{
  id: 1,
  domain: DOMAIN
}]; 

var INDEXNAV = [];
export default {
  getDUIXIN_BANNER_ID: DUIXIN_BANNER_ID,
  getDUIXIN_CATS_ID: DUIXIN_CATS_ID,
  getAPPID: DUIXIN_APPID,
  getAPPSECRET: DUIXIN_APPSECRET,
  getDomain: DOMAIN,
  getWebsiteName: WEBSITENAME,
  getPageCount: PAGECOUNT,
  getIndexNav: INDEXNAV,
  getZanImageUrl: ZANIMAGEURL,
  getDownloadFileDomain: DOWNLOADFILEDOMAIN,
  getApileixing: APILEIXING,
  getDuixinbanbenhao: DUIXINBANBEN,
  getQiandaokai: QIANDAOKAI,
  getJiansuokai: JIANSUOKAI,
  getWss: WSS,
  getH5: H5,
  getKaifazhe: KAIFAZHE
};