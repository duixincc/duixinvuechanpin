import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

var initialThemes = uni.getStorageSync('theme');  
  
if (initialThemes) {  
	var initialTheme = initialThemes;
  console.log('initialTheme:', initialTheme);  
} else {  
	var initialTheme = 'light';
  console.log('initialTheme:', initialTheme);  
}

const store = new Vuex.Store({
	
  state: {
    themeName: initialTheme,
    themeStyle: {
      "light": `
			--nav-bg:#f9f9f9; 
			--nav-color-bg:#f9f9f9;
			--nav-color:#ffffff;
			--d-beijing-color:#ffffff; 
			--d-beijing-hui:#f5f5f5;
			--d-beijing-touxiang:#ffffff;
			--d-wenziyanse-hei:#040404;
			--d-beijing-lv-d0efef:#eeeeee;
			--d-beijing-hui-f6:#f6f6f6;
			--d-yinying-10:rgba(226, 226, 226, 0.5);
			--d-img-anhei:1;
			--com1:#d2d2d2;
			--comment-sub-item:#f3f3f3;
			--seg_line:#e1e1e1;
			--d-beijing-hui-3f:#ffffff; 
			--d-wenzi-yanse-hui:#8d8e8f;
			--d-bian-1:#ebebeb;
			
		`,
      "dark": `
			--nav-bg:#000;
			--nav-color-bg:#000;
			--nav-color:#ffffff;
			--d-beijing-color:#151515;
			--d-beijing-hui:#242424;
			--d-beijing-touxiang:#393939;
			--d-wenziyanse-hei:#868686;
			--d-beijing-lv-d0efef:#2b2b2b;
			--d-beijing-hui-f6:#222222;
			--d-yinying-10:rgba(0, 0, 0, 0.5);
			--d-img-anhei:0.5;
			--com1:#353535;
			--comment-sub-item:#3f3f3f;
			--seg_line:#222222;
			--d-beijing-hui-3f:#3f3f3f; 
			--d-wenzi-yanse-hui:#8d8e8f;
			--d-bian-1:#222222;
		`
    }
  },
  getters: {
    theme(state) {
      return state.themeStyle[state.themeName]
    }
	
  },
  mutations: {
    setTheme(state, themeName = "light") {
      state.themeName = themeName;
	  getApp().globalData.zhuti(state.themeName);
    }

  }
})

export default store
