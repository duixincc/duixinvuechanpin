使用方式：
<template>
    <view>
        <view v-show="tabBarIdx==0">0</view>
        <view v-show="tabBarIdx==1">1</view>
        <view v-show="tabBarIdx==2">2</view>
        <view v-show="tabBarIdx==3">3</view>
        <view v-show="tabBarIdx==4">4</view>
        <zwTabBar :defaultSel="0" :list="tabBarList" @clickTab="clickTab" :bigIdx="2"></zwTabBar>
    </view>
</template>

<script>
    import zwTabBar from "@/components/zw-tabbar/zw-tabbar.vue";
    export default {
        components: {
            zwTabBar
        },
        data() {
            return {
                tabBarIdx: 0,
                tabBarList: [{
                        // "isTabBar":true,//是tabBar页面
                        // "pagePath": "/pages/home",
                        "iconPath": "/static/icon/tabBar/1.png",
                        "selectedIconPath": "/static/icon/tabBar/1_on.png",
                        "text": "首页"
                    }, {
                        // "pagePath": "/pages/home",
                        "iconPath": "/static/icon/tabBar/2.png",
                        "selectedIconPath": "/static/icon/tabBar/2_on.png",
                        "text": "社区"
                    },
                    {
                        // "pagePath": "/pages/home",
                        "iconPath": "/static/icon/tabBar/3.png",
                        "selectedIconPath": "/static/icon/tabBar/3_on.png",
                        "text": "搜搜"
                    },
                    {
                        // "pagePath": "/pages/home",
                        "iconPath": "/static/icon/tabBar/4.png",
                        "selectedIconPath": "/static/icon/tabBar/4_on.png",
                        "text": "新闻"
                    }, {
                        // "pagePath": "/pages/home",
                        "iconPath": "/static/icon/tabBar/5.png",
                        "selectedIconPath": "/static/icon/tabBar/5_on.png",
                        "text": "我的"
                    }
                ]
            }
        },
        onLoad() {

        },
        methods: {
            clickTab(idx) {
                this.tabBarIdx = idx;
                console.log(idx)
            }
        }
    }
</script>